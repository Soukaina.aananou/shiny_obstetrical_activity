# AANANOU Soukaina
# OUDDAROUR Ikram

##########data

cesariennes        = read.csv("cesariennes.csv",        header = TRUE, dec = ".", sep = ";")
mode_accouchement  = read.csv("mode_accouchement.csv",  header = TRUE, dec = ".", sep = ";")
mode_accouchement2 = read.csv("mode_accouchement2.csv", header = TRUE, dec = ".", sep = ";")
episiotomie        = read.csv("episiotomie.csv",        header = TRUE, dec = ".", sep = ";")
interventions      = read.csv("interventions.csv",      header = TRUE, dec = ".", sep = ";")
HPP                = read.csv("post_partum.csv",        header = TRUE, dec = ".", sep = ";")
cesa               = read.csv("cesa.csv",               header = TRUE, dec = ".", sep = ";")
cesa2              = read.csv("cesa2.csv",              header = TRUE, dec = ".", sep = ";")
evolution          = read.csv("evolution_annee.csv",    header = TRUE, dec = ".", sep = ";")

####Data necessaire pour graphique

data1 = data.frame("type accouchement"= c("nombre cesarienne","nombre voie basse",
                                          "nombre IMG","total"), 
                   "nombre" = c( mode_accouchement[25,3], mode_accouchement[25,4]
                                 , mode_accouchement[25,2],
                                 sum(mode_accouchement[25,3],
                                     mode_accouchement[25,4], 
                                     mode_accouchement[25,2])))

stat = data.frame("Déclenchement" = mode_accouchement2[25,3],
                  "Césariennes "= mode_accouchement[25,3],
                  "AccouchementInstrumental" = sum(interventions[25,4:7]),
                  "Episiotomie" = 77,
                  "HPP>1000 ml"= sum(HPP[25,c(3,6)]))

write.csv2(data1, 
           "data1.csv", 
           row.names=FALSE)
data1=read.csv("data1.csv",header=TRUE, dec=".", sep=";")

write.csv2(stat, 
           "stat.csv", 
           row.names=FALSE)
stat = read.csv("stat.csv",header=TRUE, dec=".", sep=";")

##### 
########Data code couleur cesarienne

cesa_code = data.frame("cesariennes" = c("Avant travail", "En cours de travail"),
                       "code_couleur" = c("code_vert", "code_orange","code_rouge"),
                       "frequence" = c(23, 22, 6, 41, 18, 16))
write.csv2(cesa_code, 
           "cesa_code.csv", 
           row.names=FALSE)
cesa_code = read.csv(cesa_code,header=TRUE, dec=".", sep=";")

#######################
########Data Déclenchement vs SPONTANE 

decl_spon = 
  mode_accouchement2[c(1,3,5,7,9,11,13,15,17,19,21,23),c(1,2,3)]

decl_spon$mois_2021 = c("01-01-2021","02-02-2021","03-03-2021","04-04-2021",
                        "05-05-2021","06-06-2021","07-07-2021",
                        "08-08-2021","09-09-2021","10-10-2021",
                        "11-11-2021","12-12-2021")

decl_spon$mois_2021 = as.Date(decl_spon$mois_2021, format = "%d-%m-%Y")

str(decl_spon)

################################################################################

#####Calcul des nombres demandés

#NOMBRE accouchement 2021 
mode_accouchement[25,5] #5217

#NOMBRE DE DECLENCHEMENT 
mode_accouchement2[25,3] #1657

#NOMBRE CESARIENNE 
mode_accouchement[25,3] #1079

#Nombre d'accouchement instrumental
sum(interventions[25,4:7]) #880

#Nombre d'episiotomie
episiotomie[25,2] #77

#Nombre de HPP > 1000 ml (111+114)
sum(HPP[25,c(3,6)]) #147

#####Graphes

data1 %>%
  ggplot(aes(x = type.accouchement, y = nombre, fill = type.accouchement )) +
  geom_bar(position="dodge", stat="identity") +
  ylab("frequence") + xlab("") + theme(legend.position="none")

#### EVOLUTION ACCOUCHEMENT DURANT LES 17 DERNIeRES ANNEE 
evolution %>%
  ggplot(aes(x = date)) +
  geom_line(aes(y = total_accouchement, color = "total_accouchement"), size = 1, linetype = 1)+
  geom_point(aes(y = total_accouchement, color = "total_accouchement"), size = 2)+
  geom_line(aes( y = total_cesarienne, color = "total_cesarienne"), size = 1, linetype = 1) +
  geom_point(aes( y = total_cesarienne, color = "total_cesarienne"), size = 2) +
  geom_line(aes( y = total_voiebasse, color = "total_voiebasse"), size = 1, linetype = 1) +
  geom_point(aes( y = total_voiebasse, color = "total_voiebasse"), size = 2) +
  geom_line(aes( y = total_IMG, color = "total_IMG"), size = 1, linetype = 1) +
  geom_point(aes( y = total_IMG, color = "total_IMG"), size = 2) +
  ylab("") + xlab("Date") +
  theme_light() + 
  xlim(2005,2021)

####CESARIENNE GRAPHE####### 

cesa2 %>%
  ggplot(aes(x = cesarienne, y = frequence, fill = cesariennes.Urgence )) +
  geom_bar(position="dodge", stat="identity") +
  ylab("frequence") + xlab("")

## Nombre cesa par code couleur 

cesa_code %>%
  ggplot(aes(x = cesariennes, y = frequence, fill= code_couleur)) +
  geom_bar(position="dodge", stat="identity") +
  ylab("frequence") + xlab("") +
  theme_light()+ 
  scale_fill_manual(values = c("orange", "red", "darkgreen"))
  
###########################
### DUREE CESARIENNES 

min(cesariennes$Durée)
max(cesariennes$Durée)
mean(cesariennes$Durée)

#########################################
# DECLENCHEMENT VS SPONTANEE 

ggplot(data = decl_spon, aes(x = mois_2021)) +
  geom_line(aes(y = Déclenchement, color = "declenchement"), size = 1, linetype = 1) + 
  geom_point(aes(y = Déclenchement, color = "declenchement"), size = 2) +
  geom_line(aes(y = Spontané, color = "spontané"), size = 1, linetype = 1)+
  geom_point(aes(y = Spontané, color = "spontané"), size = 2) +
  scale_x_date(date_labels = "%B-%Y")+
  theme(axis.text.x = element_text(angle=45, hjust = 1)) + 
  scale_color_manual(values = c("darkred", "steelblue")) 

#############################################"
# Nombre episiotomie par mois 

episiotomie2 =  Nombre = episiotomie[c(1,3,5,7,9,11,13,15,17,19,21,23),c(1,2)]

episiotomie2 %>%
  ggplot(aes(x=Date, y =Nombre, fill = Date))+
  geom_bar(stat="identity")+
  theme_bw()+
  xlab("Mois") +
  ylab("nombre_episiotomie") +
  labs(fill="Mois") +
  theme_classic()+
  theme(axis.text.x=element_blank())

episiotomie2 = Nombre = episiotomie[c(1,3,5,7,9,11,13,15,17,19,21,23),c(1,2)]

write.csv2(episiotomie2, 
           "episiotomie2.csv", 
           row.names=FALSE)

episiotomie2  = read.csv("episiotomie2.csv",header=TRUE, dec=".", sep=";")

episiotomie2 %>%
  ggplot(aes(x=Date, y =Nombre, fill = Date))+
  geom_bar(stat="identity")+
  theme_bw()+
  xlab("Mois") +
  ylab("nombre_episiotomie") +
  labs(fill="Mois") +
  theme_classic()+
  theme(axis.text.x=element_blank())


