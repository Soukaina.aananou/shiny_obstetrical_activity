# AANANOU Soukaina
# OUDDAROUR Ikram

#####Packages
library(xlsx)
library(lubridate)
library(dplyr)
library(plyr)
library(stringr)
library(chron)
library(ggplot2)
library(hrbrthemes)
library(readxl)
library(openxlsx)
library(scales)

#DATA  INITIALES

cesariennes=readxl::read_xlsx("cesariennes.xlsx")
activites2021=readxl::read_xlsx("activite_2021.xlsx")

############### DATA MANAGEMENT DOC CÉSARIENNE ##################

#####Vérification des data

str(cesariennes)
str(activites2021)

#####Mise des heures en times 

cesariennes$`Debut prevue`= gsub("1899-12-31","", cesariennes$`Debut prevue`)
cesariennes$`Debut prevue`= chron::as.times(cesariennes$`Debut prevue`)

cesariennes$`Fin prevue`= gsub("1899-12-31","", cesariennes$`Fin prevue`)
cesariennes$`Fin prevue`= chron::as.times(cesariennes$`Fin prevue`)

cesariennes$`Heure Création Intervention`= gsub("1899-12-31","", cesariennes$`Heure Création Intervention`)
cesariennes$`Heure Création Intervention`= chron::as.times(cesariennes$`Heure Création Intervention`)

cesariennes$`Entree reelle`= gsub("1899-12-31","", cesariennes$`Entree reelle`)
cesariennes$`Entree reelle`= chron(times. = cesariennes$`Entree reelle`)

cesariennes$`Sortie reelle`= gsub("1899-12-31","", cesariennes$`Sortie reelle`)
cesariennes$`Sortie reelle`= chron(times. = cesariennes$`Sortie reelle`)

#####Correction des durées car certaines négatives

cesariennes$Durée[cesariennes$Durée < 0] = cesariennes$Durée[cesariennes$Durée < 0] + 1440

write.csv2(cesariennes, 
           "cesariennes.csv", 
           row.names=FALSE)

#####Création de deux tableaux pour le type des césariennes

cesa = cesariennes %>%
  dplyr::group_by(cesarienne = cesariennes$`Acte principal`,
                  cesariennes$Urgence) %>%
  dplyr::summarise(frequence = n())

cesa2 = cesa[-c(1,10,13,14,15),]
cesa2$programme= ifelse(str_detect 
                        (cesa2$cesarienne,'CESARIENNE EN COURS DE TRAVAIL '), "non", "oui")
cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = "CESARIENNE EN COURS DE TRAVAIL VERT",
                                    replacement = "CESARIENNE EN COURS DE TRAVAIL")

cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = "CESARIENNE EN COURS DE TRAVAIL CODE ORANGE",
                                    replacement = "CESARIENNE EN COURS DE TRAVAIL")
cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = "CESARIENNE EN COURS DE TRAVAIL CODE ROUGE",
                                    replacement = "CESARIENNE EN COURS DE TRAVAIL")

cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = "CESARIENNE AVANT TRAVAIL CODE VERT",
                                    replacement = "CESARIENNE programme")

cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = 	
                                      "CESARIENNE AVANT TRAVAIL CODE ROUGE",
                                    replacement = "CESARIENNE programme")

cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = 	
                                      "CESARIENNE AVANT TRAVAIL CODE ORANGE",
                                    replacement = "CESARIENNE programme")

cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = 	
                                      "CESARIENNE SIMPLE",
                                    replacement = "CESARIENNE programme")
cesa2$cesarienne = str_replace_all( cesa2$cesarienne,
                                    pattern = 	
                                      "CESARIENNE UTERUS MULTI-CICATRICIEL",
                                    replacement = "CESARIENNE programme")

write.csv2(cesa, 
           "cesa.csv", 
           row.names=FALSE)
write.csv2(cesa2, 
           "cesa2.csv", 
           row.names=FALSE)


############### DATA MANAGEMENT DOC ACTIVITÉ 2021 ################

# Fonction de lecture et formatage de table
lecture_tbl <- function(table) { 
  
  # Renommer les colonnes pourcent
  compteur <- 3
  while (compteur < nrow(table) + 3) {
    colnames(table)[compteur] <- "Pourcent"
    compteur <- compteur + 2
  }
  
  # Transposer
  table <- t(table)
  
  # Recupere nom des lignes et decale d'une colonne
  noms_lignes <- rownames(table)
  table <- cbind(noms_lignes, table)
  
  # Enleve la premiere ligne
  table <- table[2:nrow(table),]
  
  # Index lignes
  for (i in 1:nrow(table)) {
    rownames(table)[i] <- i
  }
  
  # Année et pourcent
  table[,1] <- paste(table[,1], "2021")
  
  i <- 2
  while (i < nrow(table)+1) {
    table[i,1] <- paste("Pourcent",table[i-1,1])
    i <- i + 2
  }
  
  # Convertie en pourcentage et les arrondis
  if(ncol(table)>3){
    table[, ncol(table)] <- substring(table[, ncol(table)],1,ncol(table)-1 )
    table[, ncol(table)] <- gsub(",", ".", table[, ncol(table)])
  }
  
  compteur <- 2
  while (compteur < nrow(table) + 2) {
    c <- 2
    while (c < ncol(table)+1 ) {
      x <-as.numeric(table[compteur, c])
      if(is.na(x)) {x=FALSE}else {
        if(x < 1.01){
          table[compteur, c] <- as.numeric(table[compteur, c]) * 100
          table[compteur, c] <- round(as.numeric(table[compteur, c]), digits=2)
        } }
      c <- c + 1
    }
    compteur <- compteur + 2
  }
  
  return(table)
  
}

# Ouverture table Mode accouchement
mode_accouchement <- read_excel("activite_2021.xlsx")
mode_accouchement <- mode_accouchement[2:6,]
mode_accouchement <- lecture_tbl(mode_accouchement)
colnames(mode_accouchement) <- c("Date","*", "Césarienne*", "Voie basse *", "Total :", "Péridurale *")

# Ouverture table Interventions
interventions <- read_excel("activite_2021.xlsx", skip = 10, col_names = TRUE)
interventions <- interventions[2:12,]
interventions <- lecture_tbl(interventions)
colnames(interventions) <- c("Date","Césars avt W*","Césars cours W*", "Forceps", "Spatules", "Ventouse", "Ventouse+ Forceps", "Forceps sur tête dernière", "Grande extraction","Petite extraction","VB normale","TOTAL**")

# Ouverture table Episiotomie
episiotomie <- read_excel("activite_2021.xlsx", skip = 46, col_names = TRUE)
episiotomie <- episiotomie[2:2,]
episiotomie <- lecture_tbl(episiotomie)
colnames(episiotomie) <- c("Date","Nombre")

# Ouverture table Mode d'accouchement2
mode_accouchement2 <- read_excel("activite_2021.xlsx", skip = 96, col_names = TRUE)
mode_accouchement2 <- mode_accouchement2[2:5,]
mode_accouchement2 <- lecture_tbl(mode_accouchement2)
colnames(mode_accouchement2) <- c("Date", "Spontané", "Déclenchement", "Césarienne avant travail", "Total")

# Ouverture table Hémorrragie du post partum
post_partum <- read_excel("activite_2021.xlsx", skip = 107, col_names = TRUE)

post_partum <- post_partum[2:8,]
post_partum <- lecture_tbl(post_partum)
colnames(post_partum) <- c("Date", "Césarienne 500 à 1000 ml", "Césarienne > 1000 ml", "Total Césars", "Voie basse 500 à 1000 ml","Voie basse > 1000 ml", "Total VB", "Total HPP VB et césarienne")

# -------------- Export au format csv ------------------------------------------

write.csv2(mode_accouchement, 
           "mode_accouchement.csv", 
           row.names=FALSE)

write.csv2(mode_accouchement2, 
           "mode_accouchement2.csv", 
           row.names=FALSE)

write.csv2(interventions, 
           "interventions.csv", 
           row.names=FALSE)

write.csv2(post_partum, 
           "post_partum.csv", 
           row.names=FALSE)

write.csv2(episiotomie, 
           "episiotomie.csv", 
           row.names=FALSE)


