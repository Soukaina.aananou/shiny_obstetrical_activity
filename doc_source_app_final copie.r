############### DATA SOURCE ##############
cesariennes=read.csv("cesariennes.csv",header=TRUE, dec=".", sep=";")
mode_accouchement=read.csv("mode_accouchement.csv",header=TRUE, dec=".", sep=";")
mode_accouchement2=read.csv("mode_accouchement2.csv",header=TRUE, dec=".", sep=";")
episiotomie=read.csv("episiotomie.csv",header=TRUE, dec=".", sep=";")
interventions=read.csv("interventions.csv",header=TRUE, dec=".", sep=";")
HPP=read.csv("post_partum.csv",header=TRUE, dec=".", sep=";")
cesa=read.csv("cesa.csv",header=TRUE, dec=".", sep=";")
cesa2=read.csv("cesa2.csv",header=TRUE, dec=".", sep=";")
data1=read.csv("data1.csv",header=TRUE, dec=".", sep=";")
cesa_code = read.csv("cesa_code.csv",header=TRUE, dec=".", sep=";")
stat = read.csv("stat.csv",header=TRUE, dec=".", sep=";")
episiotomie2  = read.csv("episiotomie2.csv",header=TRUE, dec=".", sep=";")
evolution = read.csv("evolution_annee.csv",    header = TRUE, dec = ".", sep = ";")


